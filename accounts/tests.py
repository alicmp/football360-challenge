import json
from django.test import TestCase
from rest_framework.test import APIClient
from django.conf import settings
from rest_framework import status
from model_bakery import baker


# initialize the APIClient app
client = APIClient()


class AccountsAPITest(TestCase):
    def setUp(self):
        self.user = baker.make(settings.AUTH_USER_MODEL, is_active=True)

    def test_create_user_and_getting_token(self):
        url = '/api/v1/accounts/auth/users/'
        payload = {
            "first_name": "string",
            "last_name": "string",
            "email": "user@example.com",
            "phone": "09122224210",
            "password": "secure123",
            "re_password": "secure123"
        }
        res = client.post(url, data=json.dumps(payload),
                          content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_create_token(self):
        self.user.set_password("secure123")
        self.user.save()

        url = '/api/v1/accounts/auth/jwt/create/'
        payload = {
            "phone": self.user.phone,
            "password": "secure123",
        }
        res = client.post(url, data=json.dumps(payload),
                          content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertIn('refresh', str(res.data))
        self.assertIn('access', str(res.data))
