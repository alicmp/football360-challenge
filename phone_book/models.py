from django.conf import settings
from django.core.validators import RegexValidator
from django.db import models


class Contact(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class EmailAddress(models.Model):
    contact = models.ForeignKey('Contact', on_delete=models.CASCADE,
                                related_name='emails')
    email = models.EmailField(max_length=255)

    def __str__(self):
        return f'{self.contact} - {self.email}'


class PhoneNumber(models.Model):
    contact = models.ForeignKey('Contact', on_delete=models.CASCADE,
                                related_name='phones')
    phone = models.CharField(
        max_length=11,
        validators=[
            RegexValidator(
                regex=r'^09\d{9}$',
                message="Wrong phone number format.",
            ),
        ],
    )

    def __str__(self):
        return f'{self.contact} - {self.phone}'


class ContactGroup(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_delete=models.CASCADE)
    title = models.CharField(max_length=256)
    contacts = models.ManyToManyField('Contact', related_name='groups',
                                      blank=True)

    def __str__(self):
        return self.title


class Block(models.Model):
    contact = models.OneToOneField('Contact', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.contact.name} - {self.user}"
