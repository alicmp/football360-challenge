from django.db import IntegrityError
from rest_framework import serializers

from phone_book import models as phone_book_models


class EmailAddressSerializer(serializers.ModelSerializer):
    """Handle retriving email address"""

    class Meta:
        model = phone_book_models.EmailAddress
        fields = [
            'id',
            'email',
            'contact',
        ]
        extra_kwargs = {'contact': {'write_only': True}}


class PhoneNumberSerializer(serializers.ModelSerializer):
    """Handle retriving phone number"""

    class Meta:
        model = phone_book_models.PhoneNumber
        fields = [
            'id',
            'phone',
            'contact',
        ]
        extra_kwargs = {'contact': {'write_only': True}}


class ContactGroupMinInfoSerializer(serializers.ModelSerializer):
    """Handle retriving minimum necessary information of contact group"""

    class Meta:
        model = phone_book_models.ContactGroup
        fields = [
            'id',
            'title',
        ]


class ContactSerializer(serializers.ModelSerializer):
    """Handle retriving contact information"""

    emails = EmailAddressSerializer(read_only=True, many=True)
    phones = PhoneNumberSerializer(read_only=True, many=True)
    groups = ContactGroupMinInfoSerializer(read_only=True, many=True)

    class Meta:
        model = phone_book_models.Contact
        fields = [
            'id',
            'name',
            'emails',
            'phones',
            'groups',
        ]


class ContactGroupReadSerializer(serializers.ModelSerializer):
    """Handle creating contact group information"""

    contacts = ContactSerializer(read_only=True, many=True)

    class Meta:
        model = phone_book_models.ContactGroup
        fields = [
            'id',
            'title',
            'contacts',
        ]
        depth = 1


class ContactGroupWriteSerializer(serializers.ModelSerializer):
    """Handle creating contact group information"""

    class Meta:
        model = phone_book_models.ContactGroup
        fields = [
            'id',
            'title',
            'contacts',
        ]
        extra_kwargs = {'contacts': {'write_only': True}}


class BlockSerializer(serializers.ModelSerializer):
    """Handle blocking a contact"""

    class Meta:
        model = phone_book_models.Block
        fields = [
            'id',
            'contact',
            'user',
        ]
        extra_kwargs = {'contacts': {'read_only': True}}
