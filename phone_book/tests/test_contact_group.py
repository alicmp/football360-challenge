import json
from django.test import TestCase
from rest_framework.test import APIClient
from django.conf import settings
from django.urls import reverse
from rest_framework import status
from model_bakery import baker

from phone_book import models
from phone_book import serializers


# initialize the APIClient app
client = APIClient()


class ContactGroupAPITest(TestCase):
    def setUp(self):
        self.user = baker.make(settings.AUTH_USER_MODEL)
        self.contact_1 = baker.make('phone_book.Contact', owner=self.user)
        self.contact_2 = baker.make('phone_book.Contact', owner=self.user)

    def test_group_create_success(self):
        client.force_authenticate(user=self.user)

        url = reverse('phone_book:contact-group-list')
        payload = {
            'title': 'family',
        }
        res = client.post(url, data=json.dumps(payload),
                          content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_group_update_success(self):
        client.force_authenticate(user=self.user)

        group = baker.make('phone_book.ContactGroup', owner=self.user,
                           contacts=[self.contact_1, self.contact_2])
        kwargs = {'pk': group.pk}
        url = reverse('phone_book:contact-group-detail', kwargs=kwargs)
        payload = {
            'title': 'family',
            'contacts': [self.contact_1.id]
        }

        res = client.put(url, data=json.dumps(payload),
                         content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_group_retrieve(self):
        client.force_authenticate(user=self.user)

        group = baker.make('phone_book.ContactGroup', owner=self.user,
                           contacts=[self.contact_1, self.contact_2])
        kwargs = {'pk': group.pk}
        url = reverse('phone_book:contact-group-detail', kwargs=kwargs)

        res = client.get(url, content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_200_OK)
