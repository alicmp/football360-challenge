import json
from django.test import TestCase
from rest_framework.test import APIClient
from django.conf import settings
from django.urls import reverse
from rest_framework import status
from model_bakery import baker

from phone_book import models
from phone_book import serializers


# initialize the APIClient app
client = APIClient()


class EmailAndPhoneAPITest(TestCase):
    def setUp(self):
        self.user = baker.make(settings.AUTH_USER_MODEL)
        self.contact = baker.make('phone_book.Contact', owner=self.user)

    def test_email_address_create_success(self):
        client.force_authenticate(user=self.user)

        url = reverse('phone_book:email-address-list')
        payload = {
            'email': 'test@gmail.com',
            'contact': self.contact.pk
        }
        res = client.post(url, data=json.dumps(payload),
                          content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_email_address_bulk_create_success(self):
        client.force_authenticate(user=self.user)

        url = reverse('phone_book:email-address-list')
        payload = [
            {
                'email': 'test@gmail.com',
                'contact': self.contact.pk
            },
            {
                'email': 'test2@gmail.com',
                'contact': self.contact.pk
            },
        ]
        res = client.post(url, data=json.dumps(payload),
                          content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_email_address_create_unsuccess(self):
        new_user = baker.make(settings.AUTH_USER_MODEL)
        client.force_authenticate(user=new_user)

        url = reverse('phone_book:email-address-list')
        payload = {
            'email': 'test@gmail.com',
            'contact': self.contact.pk
        }
        res = client.post(url, data=json.dumps(payload),
                          content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)

    def test_phone_number_update(self):
        client.force_authenticate(user=self.user)

        obj = baker.make('phone_book.PhoneNumber', contact=self.contact,
                         phone='09133333333')
        kwargs = {'pk': obj.pk}
        url = reverse('phone_book:phone-number-detail', kwargs=kwargs)
        payload = {'phone': '09122222222'}

        res = client.patch(url, data=json.dumps(payload),
                           content_type='application/json')
        obj.refresh_from_db()
        serializer = serializers.PhoneNumberSerializer(obj)
        self.assertEqual(res.data, serializer.data)
