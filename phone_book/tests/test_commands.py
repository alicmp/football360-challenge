from io import StringIO

from django.core.management import call_command
from django.test import TestCase
from django.conf import settings
from model_bakery import baker
from datetime import timedelta
from django.utils import timezone
from unittest.mock import patch, Mock


class ManagementCommandTest(TestCase):
    def call_command(self, *args, **kwargs):
        out = StringIO()
        call_command(
            "report",
            *args,
            stdout=out,
            stderr=StringIO(),
            **kwargs,
        )
        return out.getvalue()

    def setUp(self):
        self.user = baker.make(settings.AUTH_USER_MODEL)

    def test_send_email(self):
        today = timezone.now()
        with patch('django.utils.timezone.now', Mock(return_value=today)):
            baker.make('phone_book.Contact', owner=self.user)
            baker.make('phone_book.Contact', owner=self.user)

        yesterday = timezone.now() - timedelta(1)
        with patch('django.utils.timezone.now', Mock(return_value=yesterday)):
            baker.make('phone_book.Contact', owner=self.user)

        with patch('phone_book.management.commands.report.send_mail', Mock(return_value=True)):
            out = self.call_command()
            self.assertEqual(out, 'E-mail Report was sent.\n')
