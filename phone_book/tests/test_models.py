from django.test import TestCase
from django.conf import settings
from model_bakery import baker

from phone_book import models


class ModelTest(TestCase):
    def setUp(self):
        self.user = baker.make(settings.AUTH_USER_MODEL)

    def test_create_contact(self):
        baker.make('phone_book.Contact', owner=self.user)
        contacts = models.Contact.objects.all()
        self.assertNotEqual(contacts.count(), 0)

    def test_create_contact_email_address(self):
        contact = baker.make('phone_book.Contact', owner=self.user)
        baker.make('phone_book.EmailAddress', contact=contact)
        contact.refresh_from_db()
        self.assertNotEqual(contact.emails.count(), 0)

    def test_create_contact_phone_number(self):
        contact = baker.make('phone_book.Contact', owner=self.user)
        baker.make('phone_book.PhoneNumber', contact=contact)
        contact.refresh_from_db()
        self.assertNotEqual(contact.phones.count(), 0)

    def test_create_contact_group(self):
        contact = baker.make('phone_book.Contact', owner=self.user)
        group = baker.make('phone_book.ContactGroup', owner=self.user)
        group.contacts.add(contact)
        contact.refresh_from_db()
        self.assertNotEqual(contact.groups.count(), 0)
        self.assertNotEqual(group.contacts.count(), 0)

    def test_create_block_model(self):
        contact = baker.make('phone_book.Contact', owner=self.user)
        block = baker.make('phone_book.Block', user=self.user, contact=contact)

        self.assertNotEqual(models.Block.objects.all().count(), 0)
