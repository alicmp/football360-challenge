import json
from django.test import TestCase
from rest_framework.test import APIClient
from django.conf import settings
from django.urls import reverse
from rest_framework import status
from model_bakery import baker

from phone_book import models
from phone_book import serializers


# initialize the APIClient app
client = APIClient()


class ContactAPITest(TestCase):
    def setUp(self):
        self.user = baker.make(settings.AUTH_USER_MODEL)

    def test_creating_contact(self):
        client.force_authenticate(user=self.user)

        url = reverse('phone_book:contact-list')
        payload = {'name': 'test'}
        res = client.post(url, data=json.dumps(payload),
                          content_type='application/json')

        contact = models.Contact.objects.get(name='test')
        serializer = serializers.ContactSerializer(contact,
                                                   context={'request': self})
        self.assertEqual(res.data, serializer.data)

    def test_getting_contact(self):
        client.force_authenticate(user=self.user)
        contact = baker.make('phone_book.Contact', owner=self.user)

        kwargs = {'pk': contact.pk}
        url = reverse('phone_book:contact-detail', kwargs=kwargs)

        res = client.get(url, content_type='application/json')

        serializer = serializers.ContactSerializer(contact,
                                                   context={'request': self})
        self.assertEqual(res.data, serializer.data)

    def test_getting_list_of_contact(self):
        client.force_authenticate(user=self.user)
        baker.make('phone_book.Contact', owner=self.user)
        baker.make('phone_book.Contact', owner=self.user)

        url = reverse('phone_book:contact-list')

        res = client.get(url, content_type='application/json')

        contacts = models.Contact.objects.filter(owner=self.user)
        serializer = serializers.ContactSerializer(contacts, many=True,
                                                   context={'request': self})
        self.assertEqual(res.data, serializer.data)

    def test_update_contact_success(self):
        client.force_authenticate(user=self.user)
        contact = baker.make('phone_book.Contact', owner=self.user)

        kwargs = {'pk': contact.pk}
        url = reverse('phone_book:contact-detail', kwargs=kwargs)

        payload = {'name': 'new name'}
        res = client.put(url, data=json.dumps(payload),
                         content_type='application/json')

        contact.refresh_from_db()
        serializer = serializers.ContactSerializer(contact,
                                                   context={'request': self})
        self.assertEqual(res.data, serializer.data)

    def test_update_contact_unsuccess(self):
        """Trying to update another users contact should be unsaccessful"""
        client.force_authenticate(user=self.user)
        new_user = baker.make(settings.AUTH_USER_MODEL)
        contact = baker.make('phone_book.Contact', owner=new_user)

        kwargs = {'pk': contact.pk}
        url = reverse('phone_book:contact-detail', kwargs=kwargs)

        payload = {'name': 'new name'}
        res = client.put(url, data=json.dumps(payload),
                         content_type='application/json')

        self.assertEqual(res.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_contact(self):
        client.force_authenticate(user=self.user)
        contact = baker.make('phone_book.Contact', owner=self.user)

        kwargs = {'pk': contact.pk}
        url = reverse('phone_book:contact-detail', kwargs=kwargs)

        res = client.delete(url, content_type='application/json')

        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)

    def test_merge(self):
        client.force_authenticate(user=self.user)

        contact_1 = baker.make('phone_book.Contact', owner=self.user)
        contact_2 = baker.make('phone_book.Contact', owner=self.user)
        baker.make('phone_book.EmailAddress', contact=contact_2,
                   email='test@gmail.com')
        baker.make('phone_book.EmailAddress', contact=contact_1,
                   email='test@gmail.com')
        baker.make('phone_book.EmailAddress', contact=contact_1,
                   email='new@gmail.com')
        baker.make('phone_book.PhoneNumber', contact=contact_1,
                   phone='0911111111')
        baker.make('phone_book.PhoneNumber', contact=contact_1,
                   phone='0911111112')

        kwargs = {'pk': contact_2.pk}
        url = reverse('phone_book:contact-merge', kwargs=kwargs)
        res = client.post(url, content_type='application/json')

        contact_2.refresh_from_db()
        serializer = serializers.ContactSerializer(contact_2,
                                                   context={'request': self})
        self.assertEqual(res.data, serializer.data)
        self.assertIn('new@gmail.com', str(res.data))
        self.assertIn('0911111112', str(res.data))

    def test_blocking_contact(self):
        client.force_authenticate(user=self.user)

        contact = baker.make('phone_book.Contact', owner=self.user)

        url = reverse('phone_book:contact-block-list')
        payload = {'contact': contact.pk}
        client.post(url, data=json.dumps(payload),
                          content_type='application/json')

        self.assertNotEqual(models.Block.objects.all().count, 0)

    def test_getting_blocked_contact(self):
        client.force_authenticate(user=self.user)
        contact = baker.make('phone_book.Contact', owner=self.user)
        baker.make('phone_book.Contact', owner=self.user)
        baker.make('phone_book.Block', contact=contact, user=self.user)
    
        url = reverse('phone_book:contact-list')

        res = client.get(url, content_type='application/json')

        contacts = models.Contact.objects.filter(owner=self.user,
                                                 block__isnull=True)
        serializer = serializers.ContactSerializer(contacts, many=True,
                                                   context={'request': self})
        self.assertEqual(res.data, serializer.data)
