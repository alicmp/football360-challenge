from django.urls import path, include
from rest_framework import routers

from phone_book import views


router = routers.DefaultRouter()
router.register('contact/email',
                views.EmailAddressViewSet,
                basename='email-address')
router.register('contact/phone',
                views.PhoneNumberViewSet,
                basename='phone-number')
router.register('contact/group',
                views.ContactGroupViewSet,
                basename='contact-group')
router.register('contact/block',
                views.BlockViewSet,
                basename='contact-block')
router.register('contact',
                views.ContactViewSet,
                basename='contact')


urlpatterns = [
    path('', include(router.urls)),
]


app_name = 'phone_book'
