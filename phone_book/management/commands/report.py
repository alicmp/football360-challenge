from datetime import timedelta

from django.core.management import BaseCommand
from django.conf import settings
from django.utils import timezone
from django.db.models import DateField, Count
from django.db.models.functions import Cast
from django.core.mail import send_mail

from phone_book.models import Contact


today = timezone.now()
yesterday = today - timedelta(1)


class Command(BaseCommand):
    help = "Send report to user"

    def handle(self, *args, **options):
        query = Contact.objects.annotate(
            create_date=Cast('created_at', DateField())).values(
                'create_date', 'owner__email').annotate(id_count=Count('id')).order_by(
                    'create_date')

        today_contacts = query.filter(create_date=today)
        yesterday_contacts = query.filter(create_date=yesterday)

        data = {}

        for contact in today_contacts:
            email = contact.get('owner__email')
            data[email] = f'Today: {contact.get("id_count")} \n'
        for contact in yesterday_contacts:
            email = contact.get('owner__email')
            if email not in data:
                data[email] = ''
            data[email] += f'Yesterday: {contact.get("id_count")} \n'

        if data:
            for email, message in data.items():
                subject = (f"Contact Report for {today.strftime('%Y-%m-%d')}")
                send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email])

            self.stdout.write("E-mail Report was sent.")
        else:
            self.stdout.write("No contacts for today.")
