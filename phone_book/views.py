import json
from django.db.models import Q
from rest_framework import viewsets, mixins, permissions, status
from rest_framework.decorators import action
from rest_framework.response import Response

from phone_book import models as models
from phone_book import serializers


class OwnerPermission(permissions.BasePermission):
    """
    Permission class to prevent users from messing with others users data
    """
    def has_permission(self, request, view):
        if view.action in ['update', 'partial_update', 'destroy']:
            obj = view.get_object()
            contact = obj.contact
        elif view.action == 'create':
            body = json.loads(request.body)
            if isinstance(body, list):
                # TODO: Check to see all contacts id in the list are the same!
                contact_id = body[0].get('contact')
            else:
                contact_id = body.get('contact')
            try:
                contact = models.Contact.objects.get(pk=contact_id)
            except models.Contact.DoesNotExist:
                return False

        if contact.owner != request.user:
            return False
        return True


class ContactViewSet(viewsets.GenericViewSet,
                     mixins.CreateModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.DestroyModelMixin):
    """Handle CRUD functionality for Contact"""
    queryset = models.Contact.objects.all()
    serializer_class = serializers.ContactSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user,
                                    block__isnull=True)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    @action(detail=True, methods=['post'])
    def merge(self, request, pk=None):
        """
        Merge all the contacts with similar email, phone number to this contact
        """
        contact = self.get_object()

        # perform the merge!
        emails = contact.emails.values_list('email', flat=True)
        phones = contact.phones.values_list('phone', flat=True)

        other_contacts = self.get_queryset().filter(
            Q(phones__phone__in=phones) | Q(emails__email__in=emails)).exclude(
                pk=contact.pk)
        other_contacts_id = other_contacts.values_list('id', flat=True)

        models.PhoneNumber.objects.filter(
            contact__in=other_contacts_id).update(contact=contact)
        models.EmailAddress.objects.filter(
            contact__in=other_contacts_id).update(contact=contact)

        contact.refresh_from_db()
        serializer = self.serializer_class(instance=contact)
        return Response(data=serializer.data)


class EmailAddressViewSet(viewsets.GenericViewSet,
                          mixins.CreateModelMixin,
                          mixins.UpdateModelMixin,
                          mixins.DestroyModelMixin):
    """Handle CUD functionality for EmailAddress"""
    queryset = models.EmailAddress.objects.all()
    serializer_class = serializers.EmailAddressSerializer
    permission_classes = [permissions.IsAuthenticated, OwnerPermission]

    def get_serializer(self, *args, **kwargs):
        if isinstance(kwargs.get("data", {}), list):
            kwargs["many"] = True

        return super(EmailAddressViewSet, self).get_serializer(*args, **kwargs)


class PhoneNumberViewSet(viewsets.GenericViewSet,
                         mixins.CreateModelMixin,
                         mixins.UpdateModelMixin,
                         mixins.DestroyModelMixin):
    """Handle CUD functionality for PhoneNumber"""
    queryset = models.PhoneNumber.objects.all()
    serializer_class = serializers.PhoneNumberSerializer
    permission_classes = [permissions.IsAuthenticated, OwnerPermission]

    def get_serializer(self, *args, **kwargs):
        if isinstance(kwargs.get("data", {}), list):
            kwargs["many"] = True

        return super(PhoneNumberViewSet, self).get_serializer(*args, **kwargs)


class ContactGroupViewSet(viewsets.GenericViewSet,
                          mixins.CreateModelMixin,
                          mixins.RetrieveModelMixin,
                          mixins.ListModelMixin,
                          mixins.UpdateModelMixin,
                          mixins.DestroyModelMixin):
    """Handle CRUD functionality for ContactGroup"""
    queryset = models.ContactGroup.objects.all()
    serializer_class = serializers.ContactGroupReadSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)

    def get_serializer_class(self):
        if self.action in ['create', 'update', 'partial_update']:
            return serializers.ContactGroupWriteSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class BlockViewSet(viewsets.GenericViewSet,
                   mixins.CreateModelMixin,):
    """Handle blocking a user"""
    queryset = models.Block.objects.all()
    serializer_class = serializers.BlockSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
