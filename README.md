# Football360 Challenge

## Requirements
- Docker!
- Makefile

## Setup on development environment
1. Navigate to the projects directory and rename the `.env.sample` file to `.env`. 
2. Run this command: 
```docker-compose up --build```

## API Documentation
After setting up the project in development env, you can visit `localhost:8000/swagger/` to see list of APIs and their fields and explanation.

## Test
Simply execute this command to run all unittests: 
```make test```

## Improvement
- We can use bulk create for api's that accept list of items to speed up the Create time. 
- We can use select related and prefetch related to reduce number of queries when we call API's that need to perform join in database. 
- Based on my API design, first we need to create a contact, then add email and phone number lists to this contact with seperate APIs. Although this method is fine, we can create everything at once too, contact and list of its phone numbers and email addresses.
