
#
# Football360-api
# @file
# @version 0.1

makemigrations:
	docker-compose run --rm web sh -c "python manage.py makemigrations"

migrate:
	docker-compose run --rm web sh -c "python manage.py migrate"

superuser:
	docker-compose run --rm web sh -c "python manage.py createsuperuser"

test:
	docker-compose run --rm web sh -c "python manage.py test"

# end
